import Home from "./pages/home";
import { Card } from "@material-ui/core";

function App() {
  return (
    <div className="App">
      <Card>
        <Home />
      </Card>
    </div>
  );
}

export default App;
