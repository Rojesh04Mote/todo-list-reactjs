import React, { useState, useRef, useEffect } from "react";
import { TextField, IconButton } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import Todos from "../component/todos";
import SnackBar from "../component/snackbar";
import Button from "@material-ui/core/Button";

export default function Home() {
  const todoRef = useRef();
  const [todos, setTodos] = useState([]);
  const [total, setTotal] = useState(0);
  const [completed, setCompleted] = useState(0);

  const addTodo = (event) => {
    event.preventDefault();
    setTodos([
      ...todos,
      {
        task: todoRef.current.value,
        completed: false,
        createdAt: Date().toLocaleString(),
      },
    ]);
    setTotal(total + 1);
    todoRef.current.value = "";
    setMessage("Todo is added  me");
    setOpen(true);
  };

  const delTodo = (delIndex) => {
    setTodos(todos.filter((todo, index) => delIndex !== index));
    if (todos[delIndex].completed) {
      setCompleted(completed - 1);
    }
    setTotal(total - 1);
    setMessage("Todo is Deleted by me");
    setOpen(true);
  };

  const markDone = (markIndex) => {
    setTodos(
      todos.map((todo, index) => {
        if (index === markIndex) {
          todo.completed = true;
          todo.completedAt = Date().toLocaleString();
        }
        return todo;
      })
    );
    setCompleted(completed + 1);
  };

  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  useEffect(() => {
    todoRef.current.focus();
  }, []);

  return (
    <div
      style={{
        background: "yellow",
        border: "outset red",
        textAlign: "center",
        color: "Highlight",
      }}
    >
      <Button variant="contained" color="secondary" disableElevation>
        <h5>This is Rojesh Todo list Account Assistant for the day;</h5>
      </Button>
      <br />
      <Button variant="contained" color="secondary" disableElevation>
        <h3>Total:{total}</h3>
      </Button>
      <br />
      <Button variant="contained" color="secondary" disableElevation>
        <h1>Completed :{completed}</h1>
      </Button>
      <br />
      <div>
        {todos.map((todo, index) => (
          <Todos
            key={index}
            index={index}
            task={todo.task}
            createdAt={todo.createdAt}
            delTodo={delTodo}
            markDone={markDone}
            completed={todo.completed}
            completedAt={todo.completedAt}
          />
        ))}
      </div>
      <br />
      <form>
        <Button variant="contained" color="secondary" disableElevation>
          <TextField
            id="filled-basic"
            type="text"
            label=" Add todo"
            variant="filled"
            inputRef={todoRef}
          />
          <IconButton type="submit" onClick={addTodo}>
            <AddIcon></AddIcon>
          </IconButton>
        </Button>
        x
      </form>

      <SnackBar open={open} handleClose={handleClose} message={message} />
    </div>
  );
}
